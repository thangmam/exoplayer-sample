package me.infimate.exoplayersample;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by thangmam on 5/16/2017.
 */

public class PlayerActivity extends AppCompatActivity {
    private Handler mainHandler;
    private BandwidthMeter bandwidthMeter;
    private SimpleExoPlayer exoPlayer;
    private SimpleExoPlayerView playerView;

    private DataSource.Factory datasourceFactory;
    private ExtractorsFactory extractorsFactory;
    private MediaSource videoSource;
    private String videoURL;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        if (getIntent() != null) {
            showDialog();
            videoURL = getIntent().getStringExtra("url");
            playerView = (SimpleExoPlayerView) findViewById(R.id.player_view);
            initPlayer();
        }
    }

    private void initPlayer() {
        mainHandler = new Handler();
        bandwidthMeter = new DefaultBandwidthMeter();
        datasourceFactory = new DefaultDataSourceFactory(PlayerActivity.this,
                Util.getUserAgent(PlayerActivity.this, "ExoplayerSample"));
        extractorsFactory = new DefaultExtractorsFactory();
        videoSource = new ExtractorMediaSource(Uri.parse(videoURL), datasourceFactory, extractorsFactory, mainHandler, null);
        TrackSelection.Factory videoTrack = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrack);
        exoPlayer = ExoPlayerFactory.newSimpleInstance(PlayerActivity.this, trackSelector);
        playerView.setPlayer(exoPlayer);
        exoPlayer.addListener(playerEventListener);
        startPlaying();
    }

    private ExoPlayer.EventListener playerEventListener = new ExoPlayer.EventListener() {


        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest) {

        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
            System.out.println("loading video " + isLoading);
            if (!isLoading) {
                dismissDialog();
            }

        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            System.out.println("Player state changed : " + playbackState);
            if (playbackState == ExoPlayer.STATE_READY) {
                dismissDialog();
            }
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
//            error.printStackTrace();
            dismissDialog();
            if (error.getCause() instanceof TimeoutException) {
                Toast.makeText(PlayerActivity.this, "Connection timeout , please try again", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(PlayerActivity.this, "Cannot play this video, try again", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onPositionDiscontinuity() {

        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

        }
    };

    private void startPlaying() {
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.prepare(videoSource);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (exoPlayer != null) {
            try {
                exoPlayer.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        dismissDialog();
    }

    private void showDialog() {
        if (progressDialog == null) {
            progressDialog = ProgressDialog.show(PlayerActivity.this, "Loading", "Please wait");
        }
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void dismissDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
