package me.infimate.exoplayersample;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.CellSignalStrengthWcdma;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView videoUrltv;
    Button btn;

    TextView sample1, sample2, sample3;
//    ClipboardManager clipboardManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, 101);
        videoUrltv = (TextView) findViewById(R.id.videourl);
        sample1 = (TextView) findViewById(R.id.sample1);
        sample2 = (TextView) findViewById(R.id.sample2);
        sample3 = (TextView) findViewById(R.id.sample3);
        sample1.setOnClickListener(onClickListener);
        sample2.setOnClickListener(onClickListener);
        sample3.setOnClickListener(onClickListener);

        btn = (Button) findViewById(R.id.play);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (videoUrltv.getText() != null && videoUrltv.getText().toString() != null && videoUrltv.getText().toString().length() > 0) {
                    String vidUrl = videoUrltv.getText().toString();
                    if (vidUrl.startsWith("http://") || vidUrl.startsWith("https://")) {
                        Intent i = new Intent(MainActivity.this, PlayerActivity.class);
                        i.putExtra("url", vidUrl);
                        startActivity(i);
                    } else {
                        Toast.makeText(MainActivity.this, "Video url must either starts with http or https", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Enter Video url", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//            clipboardManager.setPrimaryClip(ClipData.newPlainText("videourl", ((TextView) view).getText()));
            videoUrltv.setText(((TextView) view).getText());
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_OK) {

        } else {
            Toast.makeText(this, "Give permission to access internet inorder to play videos online", Toast.LENGTH_SHORT).show();
        }
    }
}

